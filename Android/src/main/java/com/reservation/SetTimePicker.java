package com.reservation;

import android.app.TimePickerDialog;
import android.os.Bundle;
import android.app.Fragment;
import android.text.format.DateFormat;
import android.widget.TextView;
import android.support.v4.app.DialogFragment;
import android.app.Dialog;
import java.util.Calendar;
import android.widget.TimePicker;
import android.widget.EditText;
import android.content.Context;
import java.util.Calendar;

public class SetTimePicker extends DialogFragment implements TimePickerDialog.OnTimeSetListener
{
	EditText et;

	public SetTimePicker(EditText et) {
		this.et = et;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState){
		final Calendar c = Calendar.getInstance();
		int hour = c.get(Calendar.HOUR_OF_DAY);
		int minute = c.get(Calendar.MINUTE);
		return new TimePickerDialog(getActivity(), this, hour, minute, DateFormat.is24HourFormat(getActivity()));
	}

	public void onTimeSet(TimePicker view, int hourOfDay, int minute){
		String ampm;
		if(hourOfDay >= 12) {
			if(hourOfDay == 12) {
				hourOfDay = 12;
			}
			else {
				hourOfDay = hourOfDay - 12;
			}
			ampm = " PM";
		}
		else {
			ampm = " AM";
		}
		String hour = hourOfDay < 10 ? "0" + Integer.toString(hourOfDay) : Integer.toString(hourOfDay);
		String min = minute < 10 ? "0" + Integer.toString(minute) : Integer.toString(minute);
		et.setText(hour + " : " + min + ampm);
	}
}
