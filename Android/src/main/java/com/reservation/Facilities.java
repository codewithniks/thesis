package com.reservation;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import com.squareup.picasso.Picasso;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import 	android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import 	android.graphics.Color;

public class Facilities extends AppCompatActivity {

	private RecyclerView recyclerView;
	private DatabaseReference myref;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_facilities);
		
		//Appbar
		setTitle("Facilities");
		Toolbar toolbar = (Toolbar) findViewById(R.id.rr_appbar);
		setSupportActionBar(toolbar);

		//handle form activity
		Button reserve = (Button) findViewById(R.id.reserveBtn);
		reserve.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(Facilities.this, Form.class));
			}
		});

		//fetch facilities from firebase
		myref = FirebaseDatabase.getInstance().getReferenceFromUrl
				("https://reservation-1d176.firebaseio.com/").child("Facilities");
		recyclerView = (RecyclerView) findViewById(R.id.recyclerview);

		LinearLayoutManager layoutManager = new LinearLayoutManager(this);
		recyclerView.setLayoutManager(layoutManager);
		recyclerView.setHasFixedSize(true);

		FirebaseRecyclerAdapter<Feed, FeedViewHolder> recyclerAdapter = new
				FirebaseRecyclerAdapter<Feed, FeedViewHolder>(
				Feed.class,
				R.layout.individual_row,
				FeedViewHolder.class,
				myref
		){
			@Override
			protected void populateViewHolder(FeedViewHolder viewHolder, Feed model, int
					position) {
				viewHolder.setName(model.getName());
				viewHolder.setLocation(model.getLocation());
				viewHolder.setDescription(model.getDescription());
				viewHolder.setImage(model.getImage());
			}
		};
		recyclerView.setAdapter(recyclerAdapter);
	}

	public static class FeedViewHolder extends RecyclerView.ViewHolder {
		View mView;
		TextView textView_name;
		TextView textView_loc;
		TextView textView_desc;
		ImageView imageView;

		public FeedViewHolder(View itemView) {
			super(itemView);
			mView = itemView;
			textView_name = (TextView) itemView.findViewById(R.id.name);
			textView_loc = (TextView) itemView.findViewById(R.id.location);
			textView_desc = (TextView) itemView.findViewById(R.id.description);
			imageView = (ImageView) itemView.findViewById(R.id.image);
		}

		public void setName(String name) {
			textView_name.setText(name);
		}

		public void setLocation(String location) {
			textView_loc.setText(location);
		}

		public void setDescription(String description) {
			textView_desc.setText(description);
		}

		public void setImage(String Image) {
			Picasso.with(mView.getContext())
					.load(Image)
					.into(imageView);
		}
	}

	//handle logout dropdown
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.options_menu, menu);

		int positionOfMenuItem = 0;
		MenuItem item = menu.getItem(positionOfMenuItem);
		SpannableString s = new SpannableString("Logout");
		s.setSpan(new ForegroundColorSpan(Color.WHITE), 0, s.length(), 0);
		item.setTitle(s);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.logoutBtn:
				FirebaseAuth.getInstance().signOut();
				startActivity(new Intent(Facilities.this, Login.class)); //Go back to home page
				finish();
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onBackPressed() {
		moveTaskToBack(true);
	}
}
