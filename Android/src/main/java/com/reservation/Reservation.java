package com.reservation;

public class Reservation 
{

	private String facility, eventTitle, date, timeStart, timeEnd;
	private String reserverNumber, attendeesCount, userEmail, userFname, userLname;

	public Reservation() {
	}

	public Reservation(String facility, String eventTitle, String date, String timeStart, String timeEnd, String reserverNumber, String attendeesCount, String userEmail, String userFname, String userLname) {
		this.facility = facility;
		this.eventTitle = eventTitle;
		this.date = date;
		this.timeStart = timeStart;
		this.timeEnd = timeEnd;
		this.reserverNumber = reserverNumber;
		this.attendeesCount = attendeesCount;
		this.userEmail = userEmail;
		this.userFname = userFname;
		this.userLname = userLname;
	}

	public String getFacility() {
		return facility;
	}

	public String getEventTitle() {
		return eventTitle;
	}

	public String getDate() {
		return date;
	}

	public String getTimeStart() {
		return timeStart;
	}

	public String getTimeEnd() {
		return timeEnd;
	}

	public String getReserverNumber() {
		return reserverNumber;
	}

	public String getAttendeesCount() {
		return attendeesCount;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public String getUserFname() {
		return userFname;
	}

	public String getUserLname() {
		return userLname;
	}
}
