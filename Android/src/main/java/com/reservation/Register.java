package com.reservation;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.view.View;
import android.content.Intent;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.text.TextUtils;
import android.graphics.Color;

import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import android.support.annotation.NonNull;
import android.util.Log;

public class Register extends AppCompatActivity
{
	private final String TAG = "Register";
	private TextView signIn;
	private Button uidBtn, registerBtn;
	private FirebaseAuth mAuth;
	private DatabaseReference db;
	private EditText userEmail, userPass, uidText, fName, lName, course, yrLvl;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);

		//DATABASE CONNECTION
		db = FirebaseDatabase.getInstance().getReference("Users");

		userEmail = (EditText) findViewById(R.id.email);
		userPass = (EditText) findViewById(R.id.password);
		fName = (EditText) findViewById(R.id.firstName);
		lName = (EditText) findViewById(R.id.lastName);
		course = (EditText) findViewById(R.id.course);
		yrLvl = (EditText) findViewById(R.id.yearLvl);

		//declaring signin link
		signIn = (TextView) findViewById(R.id.signin);
		signIn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(Register.this, Login.class));
			}
		});

		//get uid
		uidBtn = (Button) findViewById(R.id.getUidBtn);
		uidBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				String email = String.valueOf(userEmail.getText());
				String password = String.valueOf(userPass.getText());
				mAuth = FirebaseAuth.getInstance();
				mAuth.createUserWithEmailAndPassword(email, password)
					.addOnCompleteListener(new OnCompleteListener<AuthResult>() {
						@Override
						public void onComplete(@NonNull Task<AuthResult> task) {
							if(userEmail.getText().toString().length() == 0) {
								userEmail.setError("Email Address field is required");
							}
							else if(userPass.getText().toString().length() == 0) {
								userPass.setError("Password is required");
							}
							else {
								if (task.isSuccessful()) {
								Log.d(TAG, "createUserWithEmail:success");
								FirebaseUser user = mAuth.getCurrentUser();
								updateUI(user);
								} 
								else {
									Log.w(TAG, "createUserWithEmail:failure", task.getException());
									Toast.makeText(Register.this, "Registration failed.",
											Toast.LENGTH_SHORT).show();
									updateUI(null);
								}
							}
						}
					});
			}
		});

		//declaring register button
		registerBtn = (Button) findViewById(R.id.registerBtn);
		registerBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				registerUser();
				emptyFields();
			}
		});
	}

	public void updateUI(FirebaseUser user) {
		if (user != null) {
			uidText = (EditText) findViewById(R.id.uidText);
			uidText.setText(user.getUid());
		}
		else {
			return;
		}
	}

	public void registerUser() {
		String fn = fName.getText().toString();
		String ln = lName.getText().toString();
		String em = userEmail.getText().toString();
		String id = uidText.getText().toString();
		String c = course.getText().toString();
		String yl = yrLvl.getText().toString();

		Person p = new Person(fn, ln, em, c, yl);
		db.child(id).setValue(p);
		Toast.makeText(this, "Registration successful.", Toast.LENGTH_LONG).show();
		startActivity(new Intent(Register.this, Facilities.class));
	}

	public void emptyFields() {
		userEmail.setText("");
		userPass.setText("");
		uidText.setText("");
		fName.setText("");
		lName.setText("");
		course.setText("");
		yrLvl.setText("");
	}
}
