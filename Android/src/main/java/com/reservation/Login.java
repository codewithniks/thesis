package com.reservation;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class Login extends AppCompatActivity 
{

	private EditText inputEmail, inputPassword;
	private FirebaseAuth auth;
	private ProgressBar progressBar;
	private Button btnLogin, btnRegister;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);

		inputEmail = (EditText) findViewById(R.id.email);
		inputPassword = (EditText) findViewById(R.id.password);
		progressBar = (ProgressBar) findViewById(R.id.progressBar);
		btnLogin = (Button) findViewById(R.id.loginBtn);
		btnRegister = (Button) findViewById(R.id.regBtn);

		//Get Firebase auth instance
		auth = FirebaseAuth.getInstance();
		
		if (auth.getCurrentUser() != null) {
			startActivity(new Intent(Login.this, Facilities.class));
			finish();
		}

		//handle Facilities activity
		btnLogin.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String email = inputEmail.getText().toString();
				final String password = inputPassword.getText().toString();

				if (TextUtils.isEmpty(email)) {
					Toast.makeText(getApplicationContext(), "Enter email address!", Toast.LENGTH_SHORT).show();
					return;
				}

				if (TextUtils.isEmpty(password)) {
					Toast.makeText(getApplicationContext(), "Enter password!", Toast.LENGTH_SHORT).show();
					return;
				}

				progressBar.setVisibility(View.VISIBLE);

				//authenticate user
				auth.signInWithEmailAndPassword(email, password).addOnCompleteListener(Login.this, new OnCompleteListener<AuthResult>() {
					@Override
					public void onComplete(@NonNull Task<AuthResult> task) {
						progressBar.setVisibility(View.GONE);
						if (!task.isSuccessful()) {
							if (password.length() < 6) {
								inputPassword.setError(getString(R.string.minimum_password));
							} else {
								Toast.makeText(Login.this, getString(R.string.auth_failed), Toast.LENGTH_LONG).show();
							}
						} else {
							Intent intent = new Intent(Login.this, Facilities.class);
							startActivity(intent);
							finish();
						}
					}
				});
			}
		});
		btnRegister.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(Login.this, Register.class));
			}
		});
	}
}
