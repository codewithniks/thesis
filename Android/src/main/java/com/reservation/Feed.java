package com.reservation;

public class Feed 
{
	private String name,location,description,image;

	public Feed() {
	}

	public Feed(String name, String location, String description, String image) {
		this.name = name;
		this.location = location;
		this.description = description;
		this.image = image;
	}

	public String getName() {
		return name;
	}

	public String getLocation() {
		return location;
	}

	public String getDescription() {
		return description;
	}

	public String getImage() {
		return image;
	}
}
