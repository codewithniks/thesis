package com.reservation;

import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import java.text.DateFormat;
import java.util.Calendar;
import android.widget.DatePicker;
import android.app.DatePickerDialog;

import java.util.ArrayList;
import java.util.List;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;
import android.support.v4.app.FragmentManager;
import android.text.InputType;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ChildEventListener;


public class Form extends AppCompatActivity implements DatePickerDialog.OnDateSetListener,
		OnItemSelectedListener 
{

	private Spinner spinner;
	private TextView inputDate, adminEmail, adminNum;
	private EditText eventName, timeStart, timeEnd, mobileNum, attendeeCnt, inputEmail, inputFn, inputLn;
	private Button submit;
	private DatabaseReference db, adminData;
	private ArrayList<String> categories = null;
	private SetTimePicker stp;
	private FragmentManager fm;
	private Calendar cal;
	private int hour;
	private int minute;
	private int ampm;
	private FirebaseUser user;
	private String item, uid, facility, date, event, start, end, count, contact, em, fn, ln, adNum;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_form);
		//initialize ArrayList for Facilities dropdown
		categories = new ArrayList<String>();
		categories.add("Select Facility");
		//Appbar
		setTitle("Reservation Form");
		Toolbar toolbar = (Toolbar) findViewById(R.id.rr_appbar);
		setSupportActionBar(toolbar);
		//up button
		ActionBar ab = getSupportActionBar();
		ab.setDisplayHomeAsUpEnabled(true);
		//assigning new instance for FragmentManager
		fm = getSupportFragmentManager();
		//assigning new instances for variables needed for timepicker
		cal = Calendar.getInstance();
		hour = cal.get(Calendar.HOUR_OF_DAY);
		minute = cal.get(Calendar.MINUTE);
		ampm = cal.get(Calendar.AM_PM);
		//declare fields in form
		eventName = (EditText) findViewById(R.id.eventTitle);
		timeStart = (EditText) findViewById(R.id.input_tStart);
		timeEnd = (EditText) findViewById(R.id.input_tEnd);
		mobileNum = (EditText) findViewById(R.id.input_number);
		attendeeCnt = (EditText) findViewById(R.id.attendees);
		inputEmail = (EditText) findViewById(R.id.input_email);
		inputFn = (EditText) findViewById(R.id.input_fName);
		inputLn = (EditText) findViewById(R.id.input_lName);
		adminEmail = (TextView) findViewById(R.id.adminEmail);
		adminNum = (TextView) findViewById(R.id.adminNum);
		//assigning initial values for timepicker edittexts
		if(hour >= 12) {
			if(hour == 12) {
				hour = 12;
			}
			else {
				hour = hour - 12;
			}
		}
		String time = (hour < 10 ? "0" + hour : hour) + ":" + (minute < 10 ? "0" + minute : minute) + (ampm == 0 ? " AM" : " PM");
		timeStart.setText(time);
		timeEnd.setText(time);
		//make timepicker edittexts disabled and tapable/clickable
		timeStart.setInputType(InputType.TYPE_NULL);
		timeEnd.setInputType(InputType.TYPE_NULL);

		//handle time picker edit texts
		timeStart.setOnFocusChangeListener(new View.OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean onFocus) {
				if (onFocus) {
					//assign new instance of SetTimePicker Fragment
					stp = new SetTimePicker(timeStart);
					stp.show(fm.beginTransaction(), "time picker");
				}
			}
		});
		timeEnd.setOnFocusChangeListener(new View.OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean onFocus) {
				if (onFocus) {
					//assign new instance of SetTimePicker Fragment
					stp = new SetTimePicker(timeEnd);
					stp.show(fm.beginTransaction(), "time picker");
				}
			}
		});

		//handle date picker button
		ImageButton dateBtn = (ImageButton) findViewById(R.id.date_btn);
		dateBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				DialogFragment datePicker = new com.reservation.DatePicker();
				datePicker.show(getSupportFragmentManager(), "date picker");
			}
		});

		//handle drop-down
		spinner = (Spinner) findViewById(R.id.faciName);
		spinner.setOnItemSelectedListener(this);
		//get facility names
		DatabaseReference dbRef = FirebaseDatabase.getInstance().getReferenceFromUrl
				("https://reservation-1d176.firebaseio.com/").child("Facilities");
		dbRef.addChildEventListener(new ChildEventListener() {
			@Override
			public void onChildAdded(DataSnapshot dataSnapshot, String prevChildKey) {
				Feed feed = dataSnapshot.getValue(Feed.class);
				categories.add(feed.getName());
			}
			@Override
			public void onChildChanged(DataSnapshot dataSnapshot, String prevChildKey) {}
			@Override
			public void onChildRemoved(DataSnapshot dataSnapshot) {}
			@Override
			public void onChildMoved(DataSnapshot dataSnapshot, String prevChildKey) {}
			@Override
			public void onCancelled(DatabaseError databaseError) {}
		});

		//set adapter for facility dropdown
		try {
			ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);
			dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			spinner.setAdapter(dataAdapter);
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		//HANDLE SUBMIT
		submit = (Button) findViewById(R.id.submitBtn);
		submit.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				addReservation();
			}
		});

		//DATABASE CONNECTION
		db = FirebaseDatabase.getInstance().getReference("Reservations");
		//automatic fetching user email in form
		user = FirebaseAuth.getInstance().getCurrentUser();
		String email = user.getEmail();
		inputEmail.setText(email);
		//fetch names from db to form
		uid = user.getUid();
		DatabaseReference userRef = FirebaseDatabase.getInstance().getReferenceFromUrl
				("https://reservation-1d176.firebaseio.com/").child("Users");
		System.out.println("User Ref " + userRef);
		userRef.addValueEventListener(new ValueEventListener() {
			@Override
			public void onDataChange(DataSnapshot dataSnapshot) {
				String fn = dataSnapshot.child(uid).child("firstName").getValue(String.class);
				String ln = dataSnapshot.child(uid).child("lastName").getValue(String.class);
				inputFn.setText(fn);
				inputLn.setText(ln);
			}
			
			@Override
			public void onCancelled(DatabaseError databaseError) {
				System.out.println("The read failed: " + databaseError.getCode());
			}
			});

		//get adminData
		adminData = FirebaseDatabase.getInstance().getReferenceFromUrl
				("https://reservation-1d176.firebaseio.com/").child("Admin");
		adminData.addValueEventListener(new ValueEventListener() {
			@Override
			public void onDataChange(DataSnapshot dataSnapshot) {
				String adEmail =  dataSnapshot.child("email").getValue(String.class);
				adNum = dataSnapshot.child("number").getValue(String.class);
				adminEmail.setText(adEmail);
				adminNum.setText(adNum);
			}

			@Override
			public void onCancelled(DatabaseError databaseError) {
			}
		});

		//open dialer
		adminNum.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String phno="10digits";
				Intent callIntent = new Intent(Intent.ACTION_DIAL);
				callIntent.setData(Uri.parse("tel:"+ adNum ));
				startActivity(callIntent);
			}
		});
	}

	//handle date selected
	@Override
	public void onDateSet(DatePicker datePicker, int year, int month, int day) {
		Calendar c = Calendar.getInstance();
		c.set(Calendar.MONTH, month);
		c.set(Calendar.DAY_OF_MONTH, day);
		c.set(Calendar.YEAR, year);
		String pickedDate = DateFormat.getDateInstance(DateFormat.SHORT).format(c.getTime());
		inputDate = (TextView) findViewById(R.id.eventDate);
		inputDate.setText(pickedDate);
	}

	//handle drop down selection
	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
		item = parent.getItemAtPosition(position).toString();
	}

	public void onNothingSelected(AdapterView<?> arg0) {
	}

	public void emptyFields() {
		eventName.setText("");
		timeStart.setText("");
		timeEnd.setText("");
		mobileNum.setText("");
		attendeeCnt.setText("");
		inputFn.setText("");
		inputLn.setText("");
		inputDate.setText("");
	}

	private void addReservation() {
		//convert input values to String
		facility = spinner.getSelectedItem().toString();
		date = inputDate.getText().toString();
		event = eventName.getText().toString();
		start = timeStart.getText().toString();
		end = timeEnd.getText().toString();
		count = attendeeCnt.getText().toString();
		contact = mobileNum.getText().toString();
		em = inputEmail.getText().toString();
		fn = inputFn.getText().toString();
		ln = inputLn.getText().toString();
		if (!(TextUtils.isEmpty(facility) && TextUtils.isEmpty(date) && TextUtils.isEmpty(event)
				&& TextUtils.isEmpty(start) && TextUtils.isEmpty(end) && TextUtils.isEmpty(count)
				&& TextUtils.isEmpty(contact) && TextUtils.isEmpty(em) && TextUtils.isEmpty(fn)
				&& TextUtils.isEmpty(ln))) {
				//submit all data na
				String id = db.push().getKey();
				Reservation reservation = new Reservation(facility, event, date, start,
						end, contact, count, em, fn, ln);
				db.child(id).setValue(reservation);
				Toast.makeText(this, "Reservation added to system. Pending for approval.", Toast
						.LENGTH_LONG)
						.show();
				emptyFields();
		}
		else {
			Toast.makeText(this, "Please fill out all the fields", Toast.LENGTH_LONG).show();
		}
	}
}
