package com.reservation;

public class Person
{
	private String fName, lName, email, course, year;

	public Person() {
	}

	public Person(String fName, String lName, String email, String course, String year) {
		this.fName = fName;
		this.lName = lName;
		this.email = email;
		this.course = course;
		this.year = year;
	}

	public String getFirstName() {
		return fName;
	}

	public String getLastName() {
		return lName;
	}

	public String getEmail() {
		return email;
	}

	public String getCourse() {
		return course;
	}

	public String getYear() {
		return year;
	}
}
