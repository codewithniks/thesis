// Initialize Firebase
//To make sure there was no undeclared variables being use. This is in global scope.
'use strict';
const config = {
	apiKey: "AIzaSyDamF1-FdZDHSvPrUAQlOtZ2xGiqIivbRs",
    authDomain: "reservation-1d176.firebaseapp.com",
    databaseURL: "https://reservation-1d176.firebaseio.com",
    projectId: "reservation-1d176",
    storageBucket: "reservation-1d176.appspot.com",
    messagingSenderId: "997789781282"
};
firebase.initializeApp(config);
console.log(firebase);
const database = firebase.database().ref();
const rootRef  = database.child("Reservations").orderByChild("status"); 

let decline = id => {
	const userRes = database.child('Reservations/' + id);
	userRes.update({ status: 'declined' });
	console.log(`Reservation for ${id} has been declined!`);
}

let approve = id => {
	const userRes = database.child('Reservations/' + id);
	userRes.update({ status: 'approved' });
	console.log(`Reservation for ${id} has been approved!`);
}
let cancel = id => {
	const userRes = database.child('Reservations/' + id);
	userRes.update({status: 'cancelled'});
	console.log(`Reservation for ${id} has been cancelled!`)
}

rootRef.on("child_added", snap => {
	let firstname  = snap.child("firstName").val();
	let lastname  = snap.child("lastName").val();
	let email  = snap.child("email").val();
	let facility  = snap.child("facility").val();
	let dateofreservation  = snap.child("date").val();
	let timeStart  = snap.child("timeStart").val();
	let timeEnd = snap.child("timeEnd").val();
	let numberpeople  = snap.child("attendeesCount").val();
	let status = snap.child("status").val();
	let description = snap.child("eventTitle").val();
	let contact = snap.child("phoneNumber").val();
	let uid = snap.key;
	let row = `<tr><td> ${firstname} </td><td> ${lastname} </td><td> ${email} </td><td> ${contact} </td><td> ${facility} </td><td> ${description} </td><td> ${dateofreservation} </td><td> ${timeStart} </td><td> ${timeEnd} </td><td> ${numberpeople} </td>`;

	if (status === 'pending') {
		$("#table_body").append(`${row} <td><a href="#" id='approve_${uid}' onclick="window.open('https://mail.google.com/mail/u/0/?view=cm&fs=1&to=${email}&su=Room%20Reservation&body=Your%20reservation%20on%20room%20%60${facility}%60%20has%20been%20Accepted&tf=1&fbclid=IwAR3BeNn2aaTAAcG9-Q2S3UgnpWlwPwMOtZHSbmmaU7QFnbXX3AW95yQtq94')">Approve</a>  <a href="#" id='decline_${uid}' onclick="window.open('https://mail.google.com/mail/u/0/?view=cm&fs=1&to=${email}&su=Room%20Reservation&body=Your%20reservation%20on%20room%20%60${facility}%60%20has%20been%20Accepted&tf=1&fbclid=IwAR3BeNn2aaTAAcG9-Q2S3UgnpWlwPwMOtZHSbmmaU7QFnbXX3AW95yQtq94')">Decline</a></td></tr>`);
	}
	else if (status === 'declined'){
		$("#lists").append(`${row} <td> ${status} </td></tr>`);
	}
	else if (status === 'approved'){
		$("#lists").append(`${row} <td> ${status} </td><td><a href="#" id='cancel_${uid}' onclick="window.open('https://mail.google.com/mail/u/0/?view=cm&fs=1&to=${email}&su=Room%20Reservation&body=Your%20reservation%20on%20room%20%60${facility}%60%20has%20been%20Cancelled&tf=1&fbclid=IwAR3BeNn2aaTAAcG9-Q2S3UgnpWlwPwMOtZHSbmmaU7QFnbXX3AW95yQtq94')">Cancel</a></td></tr>`);
	}
	else if (status === 'cancelled'){
		$("#lists").append(`${row} <td> ${status} </td></tr>`)
	}

	let dec = document.getElementById(`decline_${uid}`);
	let appr = document.getElementById(`approve_${uid}`);
	let can = document.getElementById(`cancel_${uid}`);

	if(dec) {
		dec.addEventListener("click", () => {
			decline(uid);
			location.reload(1500);
		});
	}
	if(appr) {
		appr.addEventListener("click", () => {
			approve(uid);
			location.reload(1500);
		});
	} 	
	if (can){
		 can.addEventListener("click", () => {
			 cancel(uid);
			 location.reload(1500);
		 });
	}
});
firebase.auth().onAuthStateChanged( user => {
	if (user) {
		// User is signed in.
		document.getElementById("notSignedIn").style.display = "block";
		document.getElementById("login_div").style.display = "none";
		
	} else {
		// No user is signed in.
		document.getElementById("notSignedIn").style.display = "none";
		document.getElementById("login_div").style.display = "block";
	}
});

let login = () => {
		let userEmail  = document.getElementById("email_field").value;
		let userPass = document.getElementById("password_field").value;
		firebase.auth().signInWithEmailAndPassword(userEmail, userPass).catch( function(error){
			// Handle Errors here.
			let errorCode = error.code;
			let errorMessage = error.message;
			window.alert("Error: "+errorMessage);
			document.getElementById('email_field').reset();
			document.getElementById('password_field').reset();
		});
}

let logout = () => {
	firebase.auth().signOut().then(() => console.log("You have logout successfully!").catch(error => console.log(error)));
}